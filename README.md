# MrSkeltal

This is a proof-of-concept/weekend project to learn vertex skinning.

## Running
The code is meant to be run on Python 3.6. However, it should run on Python3.4+
if you switch to the `python3.4` branch

Because the latest release of pygame (`1.9.2`) is broken on PyPI, you have to either
install pygame from source, or use the experimental `sdl2` branch, which replaces pygame
with a SDL2-ctypes binding.

